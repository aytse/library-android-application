package com.dosamericancorner.options;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import au.com.bytecode.opencsv.CSVReader;

import com.dosamericancorner.checkout.CheckOutDataBaseAdapter;
import com.dosamericancorner.inventory.InventoryAdapter;
import com.dosamericancorner.login.HomeActivity;
import com.dosamericancorner.login.HomeScreen;
import com.dosamericancorner.login.R;
import com.dosamericancorner.membership.ManageMemberScreen;
import com.dosamericancorner.membership.MembershipAdapter;
import com.dosamericancorner.reports.ReportsByDateScreen;
import com.dosamericancorner.search.SearchScreen;
import com.dosamericancorner.statistics.StatisticsAdapter;

public class InventorySyncScreen extends Activity {
	private ProgressDialog progressDialog;
	EditText inputSource;
	Button buttonSyncInventory, buttonNewSource;
	StatisticsAdapter StatisticsAdapter;
	InventoryAdapter InventoryAdapter;
	CheckOutDataBaseAdapter CheckOutDataBaseAdapter;
	MembershipAdapter MembershipAdapter;
	Spinner spnr;
	String[] menuOptions = {
			"",
            "Manage Inventory",
            "Manage Members",
            "Settings",
            "Help",
            "Sign Off"
    };
	
	String[][] addNew(String[][] oldString, String[] newString)
	{
		String[][] tempString = Arrays.copyOf(oldString, oldString.length + 1); // New array with row size of old array + 1

		tempString[oldString.length] = new String[oldString.length]; // Initializing the new row

		// Copying data from newString array to the tempString array
		for (int i = 0; i < newString.length; i++) {
		    tempString[tempString.length-1][i] = newString[i];
		}
		
		return tempString;
	}
	
	   public void onCreate(Bundle savedInstanceState)
	   {
	      super.onCreate(savedInstanceState);
	      setContentView(R.layout.inventory_sync);
	      
		   // get Instance  of Database Adapter	
	      	MembershipAdapter=new MembershipAdapter(this);
		    MembershipAdapter=MembershipAdapter.open();
		    StatisticsAdapter=new StatisticsAdapter(this);
			StatisticsAdapter=StatisticsAdapter.open();
		    InventoryAdapter=new InventoryAdapter(this);
		    InventoryAdapter=InventoryAdapter.open();
			CheckOutDataBaseAdapter=new CheckOutDataBaseAdapter(this);
			CheckOutDataBaseAdapter=CheckOutDataBaseAdapter.open();
	      
	      Intent intent = getIntent();
	      final String userName = intent.getExtras().getString("username");
	      String source = intent.getExtras().getString("source");
		  TextView sourceText =(TextView)findViewById(R.id.textInventoryLocation);
		  if(source.equals("0"))
			  source = "/Download/LibraryThing_export.csv";
		  sourceText.setText("Please ensure the CSV file containing your book data is located in "+source);
		  final String location = source;
	      
	      // Get References of Views
	      inputSource=(EditText)findViewById(R.id.textNewSource);
	      buttonSyncInventory = (Button)findViewById(R.id.btnSyncInventory);
	      buttonNewSource = (Button)findViewById(R.id.btnNewSource);
	      
	      spnr = (Spinner)findViewById(R.id.spinnerMenu);
	        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
	                this, R.layout.menu_spinner_item, menuOptions);
	        spnr.setAdapter(adapter);
	        spnr.setOnItemSelectedListener(
	                new AdapterView.OnItemSelectedListener() {
	                    @Override
	                    public void onItemSelected(AdapterView<?> arg0, View arg1,
	                            int arg2, long arg3) {
	                        int position = spnr.getSelectedItemPosition();
	                        if(menuOptions[position].equals("Manage Inventory"))
	                        {
	                        	Intent i=new Intent(InventorySyncScreen.this,InventoryOptionScreen.class);
	        					i.putExtra("username",userName);
	        					startActivity(i);
	                        }
	                        if(menuOptions[position].equals("Manage Members"))
	                        {
	                        	Intent i=new Intent(InventorySyncScreen.this,ManageMemberScreen.class);
	        					i.putExtra("username",userName);
	        					startActivity(i);
	                        }
	                        if(menuOptions[position].equals("Settings"))
	                        {
	                        	Intent i=new Intent(InventorySyncScreen.this,SettingScreen.class);
	        					i.putExtra("username",userName);
	        					startActivity(i);
	                        }
	                        if(menuOptions[position].equals("Help"))
	                        {
	                        	Intent i=new Intent(InventorySyncScreen.this,HelpScreen.class);
	        					i.putExtra("username",userName);
	        					startActivity(i);
	                        }
	                        if(menuOptions[position].equals("Sign Off"))
	                        {
	                        	Intent intentHome=new Intent(InventorySyncScreen.this,HomeActivity.class);
	        				  	startActivity(intentHome);
	                        }
	                    }
	                    @Override
	                    public void onNothingSelected(AdapterView<?> arg0) {
	                        // TODO Auto-generated method stub
	                    }
	                }
	            );
		  
	      buttonSyncInventory.setOnClickListener(new View.OnClickListener() {
	    	  public void onClick(View arg0) {
	    		//
	    		  progressDialog = ProgressDialog.show(InventorySyncScreen.this, "", "Loading...");
	    		  			ArrayList<errorItem> errorList = new ArrayList<errorItem>();
	    		    		int successCounter = 0;
				    		  int errorCounter = 0;
				    		  String[][] errors = new String[1][6];
				    		  String[] line = new String[6];
				    		  try {
				    			  int titleCol = -1, authorCol = -1, dateCol = -1, isbnCol = -1, quantityCol = -1, tagCol = -1;
					    		  CSVReader reader = new CSVReader(new FileReader(new File(Environment.getExternalStorageDirectory().getPath()+location)));
					    		  String [] nextLine;
					    		    nextLine = reader.readNext();
					    		    int index = 0;
					    		    int lineLength = nextLine.length;
					    		    while(titleCol < 0 && index < lineLength)
					    		    {
					    		    	if(nextLine[index].contains("TITLE"))
					    		    		titleCol = index;
					    		    	index++;
					    		    }
					    		    index = 0;
					    		    while(authorCol < 0 && index < lineLength)
					    		    {
					    		    	if(nextLine[index].contains("AUTHOR"))
					    		    		authorCol = index;
					    		    	index++;
					    		    }
					    		    index = 0;
					    		    while(dateCol < 0 && index < lineLength)
					    		    {
					    		    	if(nextLine[index].contains("DATE"))
					    		    		dateCol = index;
					    		    	index++;
					    		    }
					    		    index = 0;
					    		    while(isbnCol < 0 && index < lineLength)
					    		    {
					    		    	if(nextLine[index].contains("ISBN"))
					    		    		isbnCol = index;
					    		    	index++;
					    		    }
					    		    index = 0;
					    		    while(quantityCol < 0 && index < lineLength)
					    		    {
					    		    	if(nextLine[index].contains("COPIES"))
					    		    		quantityCol = index;
					    		    	index++;
					    		    }
					    		    index = 0;
					    		    while(tagCol < 0 && index < lineLength)
					    		    {
					    		    	if(nextLine[index].contains("TAGS"))
					    		    		tagCol = index;
					    		    	index++;
					    		    }
					    		    int counter = 0;
					    		    while ((nextLine = reader.readNext()) != null) {
					    		        // nextLine[] is an array of values from the line
					    		    	/*
					    		    	 * nextLine[titleCol] = TITLE
					    		    	 * nextLine[authorCol] = AUTHOR
					    		    	 * nextLine[dateCol] = DATE
					    		    	 * nextLine[isbnCol] = ISBN
					    		    	 * nextLine[quantityCol] = QUANTITY
					    		    	 * nextLine[tagCol] = TAGS
					    		    	 */
					    		    	lineLength = nextLine.length;
					    		    	if(nextLine[isbnCol].contains("0") || nextLine[isbnCol].contains("1") 
				    		    				|| nextLine[isbnCol].contains("2") || nextLine[isbnCol].contains("3")
				    		    				|| nextLine[isbnCol].contains("4") || nextLine[isbnCol].contains("5")
				    		    				|| nextLine[isbnCol].contains("6") || nextLine[isbnCol].contains("7")
				    		    				|| nextLine[isbnCol].contains("8") || nextLine[isbnCol].contains("9"))
					    		    	{
					    		    	
						    		    	if(InventoryAdapter.isbnFoundInInventory(nextLine[isbnCol]) == false)
						    		    	{
						    		    		
						    		    		if(nextLine[dateCol].contains("0") || nextLine[dateCol].contains("1") 
						    		    				|| nextLine[dateCol].contains("2") || nextLine[dateCol].contains("3")
						    		    				|| nextLine[dateCol].contains("4") || nextLine[dateCol].contains("5")
						    		    				|| nextLine[dateCol].contains("6") || nextLine[dateCol].contains("7")
						    		    				|| nextLine[dateCol].contains("8") || nextLine[dateCol].contains("9"))
						    		    			line[2] = nextLine[dateCol];
						    		    		else
						    		    			line[2]="-9999";
						    		    		if(nextLine[quantityCol].contains("0") || nextLine[quantityCol].contains("1") 
						    		    				|| nextLine[quantityCol].contains("2") || nextLine[quantityCol].contains("3")
						    		    				|| nextLine[quantityCol].contains("4") || nextLine[quantityCol].contains("5")
						    		    				|| nextLine[quantityCol].contains("6") || nextLine[quantityCol].contains("7")
						    		    				|| nextLine[quantityCol].contains("8") || nextLine[quantityCol].contains("9"))
						    		    			line[4] = nextLine[quantityCol];
						    		    		else
						    		    			line[4]="-9999";
						    		    		if(tagCol < lineLength && nextLine[tagCol].length() > 0)
						    		    			line[5] = nextLine[tagCol];
						    		    		else
						    		    			line[5] = "*ERROR*";
						    		    		if(titleCol < lineLength && nextLine[titleCol].length() > 0)
						    		    			line[0] = nextLine[titleCol];
						    		    		else
						    		    			line[0] = "*ERROR*";
						    		    		if(authorCol < lineLength && nextLine[authorCol].length() > 0)
						    		    			line[1] = nextLine[authorCol];
						    		    		else
						    		    			line[1] = "*ERROR*";
						    		    		if(isbnCol < lineLength && nextLine[isbnCol].length() > 0)
						    		    			line[3] = nextLine[isbnCol];
						    		    		else
						    		    			line[3] = "*ERROR*";
						    		    		
						    		    		if(line[0].equals("*ERROR*") || line[1].equals("*ERROR*")
						    		    				|| line[3].equals("*ERROR*")|| line[5].equals("*ERROR*")
						    		    				|| line[2].equals("-9999")|| line[4].equals("-9999"))
						    		    		{
						    		    			errorList.add(new errorItem(line[0], line[1], Integer.parseInt(line[2]), 
						    		    					line[3], Integer.parseInt(line[4]), line[5]));
						    		    			errorCounter++;
						    		    		}
						    		    		else
						    		    		{
						    		    			InventoryAdapter.insertEntry(line[0], line[1], line[3], Integer.parseInt(line[2]),
						    		    					line[5], Integer.parseInt(line[4]), 14);
						    		    			successCounter++;
						    		    		}
						    		    	}
					    		    	}
					    		    	counter++;
					    		    	
					    		   }
				    		  } catch (FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				    		  
				    		  progressDialog.dismiss();
				    		  
				    		  Intent i=new Intent(InventorySyncScreen.this,SyncSuccessScreen.class);
								i.putExtra("username",userName);
								i.putExtra("numSuccess",successCounter);
								i.putExtra("numError", errorCounter);
								i.putExtra("errorList", errorList);
								startActivity(i);
	    		  
	    	  }
	      });
	      
	      buttonNewSource.setOnClickListener(new View.OnClickListener() {
	    	  public void onClick(View arg0) {
	    		  	String source=inputSource.getText().toString();
	   			
					// check if all of the fields are vacant
					if(source.equals(""))
					{
							Toast.makeText(getApplicationContext(), "Source Field Vaccant.", Toast.LENGTH_LONG).show();
							return;
					}
					else
					{
						/// Create Intent for HomeScreen  and Start The Activity
						Intent i=new Intent(InventorySyncScreen.this,InventorySyncScreen.class);
						i.putExtra("username",userName);
						i.putExtra("source",source);
						startActivity(i);
					}
	    	  }
	      });
	        


	      // Home Button
	      Button bh = (Button) findViewById(R.id.btnHomeBottom);
	      bh.setOnClickListener(new View.OnClickListener() {
	    	  public void onClick(View arg0) {
	    		/// Create Intent for HomeScreen  and Start The Activity
					Intent i=new Intent(InventorySyncScreen.this,HomeScreen.class);
					i.putExtra("username",userName);
					startActivity(i);
	    	  }
	      });
	      
	      // Search Button
	      Button bs = (Button) findViewById(R.id.btnSearchBottom);
	      bs.setOnClickListener(new View.OnClickListener() {
	    	  public void onClick(View arg0) {
		        /// Create Intent for SearchScreen  and Start The Activity
				Intent i=new Intent(InventorySyncScreen.this,SearchScreen.class);
				i.putExtra("username",userName);
				startActivity(i);
		         } 
	      });
	      
	      // Reports Button
	      Button br = (Button) findViewById(R.id.btnReportsBottom);
	      br.setOnClickListener(new View.OnClickListener() {
	         public void onClick(View arg0) {
	        	 Intent i = new Intent(InventorySyncScreen.this, ReportsByDateScreen.class);
	        	 i.putExtra("username",userName);
		         i.putExtra("startMonth",0);
		         i.putExtra("startYear", 0);
		         i.putExtra("endMonth", 0);
		         i.putExtra("endYear", 0);
		         startActivity(i);
	         } 
	      });
	   }

}