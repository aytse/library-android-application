package com.dosamericancorner.membership;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.dosamericancorner.login.HomeActivity;
import com.dosamericancorner.login.HomeScreen;
import com.dosamericancorner.login.R;
import com.dosamericancorner.options.HelpScreen;
import com.dosamericancorner.options.InventoryOptionScreen;
import com.dosamericancorner.options.SettingScreen;
import com.dosamericancorner.reports.ReportsByDateScreen;
import com.dosamericancorner.search.SearchScreen;

public class MemberAddScreen extends Activity{
	EditText inputFirstName,inputLastName,inputBirthday,inputMemberId,inputEmail,inputCheckoutCount,inputKarmaPts,
	inputNotes;
	Button buttonMemberAdd, buttonBack;
	MembershipAdapter MembershipAdapter;
	Spinner spnr;
	String[] menuOptions = {
			"",
            "Manage Inventory",
            "Manage Members",
            "Settings",
            "Help",
            "Sign Off"
    };
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.memberadd);
		
		MembershipAdapter = new MembershipAdapter(this);
		MembershipAdapter = MembershipAdapter.open();
		
		Intent intent = getIntent();
		final String userName = intent.getExtras().getString("username");
		
		inputFirstName = (EditText)findViewById(R.id.inputFirstName);
		inputLastName = (EditText)findViewById(R.id.inputLastName);
		inputBirthday = (EditText)findViewById(R.id.inputBirthday);
		inputMemberId = (EditText)findViewById(R.id.inputMemberId);
		inputEmail = (EditText)findViewById(R.id.inputEmail);
		inputCheckoutCount = (EditText)findViewById(R.id.inputCheckoutCount);
		inputKarmaPts = (EditText)findViewById(R.id.inputKarmaPts);
		inputNotes = (EditText)findViewById(R.id.inputNotes);
		
		buttonMemberAdd = (Button)findViewById(R.id.buttonMemberAdd);
		buttonBack=(Button)findViewById(R.id.buttonBack);
		
		buttonMemberAdd.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String firstName = inputFirstName.getText().toString();
				String lastName = inputLastName.getText().toString();
				String birthday = inputBirthday.getText().toString();
				String memberid = inputMemberId.getText().toString();
				String email = inputEmail.getText().toString();
				Integer checkoutCount = null;
				Integer karmaPts = null;
				String notes = inputNotes.getText().toString();
				if(inputCheckoutCount.getText().toString().length()>0)
					checkoutCount = Integer.parseInt(inputCheckoutCount.getText().toString());
				if(inputKarmaPts.getText().toString().length()>0)
					karmaPts = Integer.parseInt(inputKarmaPts.getText().toString());
			
				// Check vacant fields
				if(firstName.equals("") || lastName.equals("") || birthday.equals("") || memberid.equals("")
						|| email.equals("") ||checkoutCount==null || karmaPts==null || notes.equals(""))
					{
						Toast.makeText(getApplicationContext(), "Field Vaccant.", Toast.LENGTH_LONG).show();
						return;
					}
					else {
						MembershipAdapter.insertEntry(firstName, lastName, birthday, memberid, email, checkoutCount, karmaPts, notes);
					
						Toast.makeText(getApplicationContext(), "New Item Added Successfully.", Toast.LENGTH_LONG).show();
						
						Intent i = new Intent(MemberAddScreen.this, ManageMemberScreen.class);
						i.putExtra("username",userName);
						startActivity(i);
					}
			}
			
		});
		
		buttonBack.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(MemberAddScreen.this, SearchScreen.class);
		        i.putExtra("username",userName);
		        startActivity(i);
			}
	    });
		
		 spnr = (Spinner)findViewById(R.id.spinnerMenu);
	        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
	                this, R.layout.menu_spinner_item, menuOptions);
	        spnr.setAdapter(adapter);
	        spnr.setOnItemSelectedListener(
	                new AdapterView.OnItemSelectedListener() {
	                    @Override
	                    public void onItemSelected(AdapterView<?> arg0, View arg1,
	                            int arg2, long arg3) {
	                        int position = spnr.getSelectedItemPosition();
	                        if(menuOptions[position].equals("Manage Inventory"))
	                        {
	                        	Intent i=new Intent(MemberAddScreen.this,InventoryOptionScreen.class);
	        					i.putExtra("username",userName);
	        					startActivity(i);
	                        }
	                        if(menuOptions[position].equals("Manage Members"))
	                        {
	                        	Intent i=new Intent(MemberAddScreen.this,ManageMemberScreen.class);
	        					i.putExtra("username",userName);
	        					startActivity(i);
	                        }
	                        if(menuOptions[position].equals("Settings"))
	                        {
	                        	Intent i=new Intent(MemberAddScreen.this,SettingScreen.class);
	        					i.putExtra("username",userName);
	        					startActivity(i);
	                        }
	                        if(menuOptions[position].equals("Help"))
	                        {
	                        	Intent i=new Intent(MemberAddScreen.this,HelpScreen.class);
	        					i.putExtra("username",userName);
	        					startActivity(i);
	                        }
	                        if(menuOptions[position].equals("Sign Off"))
	                        {
	                        	Intent intentHome=new Intent(MemberAddScreen.this,HomeActivity.class);
	        				  	startActivity(intentHome);
	                        }
	                    }
	                    @Override
	                    public void onNothingSelected(AdapterView<?> arg0) {
	                        // TODO Auto-generated method stub
	                    }
	                }
	            );
		
	    // ============== Bottom row of buttons ==============

	      // Home Button
	      Button bh = (Button) findViewById(R.id.btnHomeBottom);
	      bh.setOnClickListener(new View.OnClickListener() {
	    	  public void onClick(View arg0) {
	    		/// Create Intent for HomeScreen  and Start The Activity
					Intent intentHome=new Intent(MemberAddScreen.this,HomeScreen.class);
					intentHome.putExtra("username",userName);
					startActivity(intentHome);
	    		  
	    	  }
	      });
	      
	      // Search Button
	      Button bs = (Button) findViewById(R.id.btnSearchBottom);
	      bs.setOnClickListener(new View.OnClickListener() {
	    	  public void onClick(View arg0) {
	    		// do nothing since user is already on home screen
		         } 
	      });
	      
	      // Reports Button
	      Button br = (Button) findViewById(R.id.btnReportsBottom);
	      br.setOnClickListener(new View.OnClickListener() {
	         public void onClick(View arg0) {
	        	 Intent i = new Intent(MemberAddScreen.this, ReportsByDateScreen.class);
	        	 i.putExtra("username",userName);
		         i.putExtra("startMonth",0);
		         i.putExtra("startYear", 0);
		         i.putExtra("endMonth", 0);
		         i.putExtra("endYear", 0);
		         startActivity(i);
	         } 
	      });
	      
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	    // Close The Database
		MembershipAdapter.close();
	}
	
}